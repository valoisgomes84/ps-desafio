# Desafio Publicsoft (Node)

## O Desafio
Fazendo uso do Framework NestJS.

  - Criar uma API REST para cadastrar, consultar, desativar e editar Processos seguindo o modelo do diagrama disponibilizado.
    - status é um enumerado: EM_ANDAMENTO, CANCELADO, CONCLUIDO.
    - O status inicial de um processo é EM_ANDAMENTO.
    - Não deve permitir alterar um processo que esteja com o status igual a CONCLUIDO.
    - O único campo do processo que deve ser possível editar é o status.
    - Ao desativar um processo, mudar seu status para CANCELADO.
    - Criar um endpoint para inserir Detalhes (processo_detalhe) de um processo.


  - Criar uma outra API REST para cadastrar, consultar, desativar e editar Documentos seguindo o modelo do diagrama disponibilizado.
    - Deve ser permitidos cadastrar múltiplos documentos com uma única requisição.
    - Ao cadastrar um ou mais documento(s), deve ser feito uso da API de Processos para criar um novo processo e vincular o processo criado ao(s)documento(s) que será cadastrado.
    - A cada documento criado deve ser inserido a tabela processo_detalhe com a descrição do documento criado.
    - Caso seja informado o código de um processo válido, não criar um novo processo e sim incrementar o processo já existente.
    - Caso um documento seja editado, adicionar a nova descrição do documento na tabela processo_detalhe.

## Obrigatório
  - Utilizar o banco de dados PostgreSQL para armazenar os dados.
  - Criar um banco de dados para cada API.
  - Utilizar typeORM.
  - As API's devem se comunicar.
  - Os registros não devem ser removidos do banco de dados, efetuar exclusão lógica.
  - Os endpoints devem obedecer a arquitetura REST.

## Diferenciais
  - Fazer uso de migration para criação do banco de dados.
  - Criar interface para cadastrar um ou mais Documentos com Angular.
  - Fazer uso do Swagger nas API's para documentação dos endpoints.

## Observações
  - Antes de começar crie uma branch com seu nome neste repositório ou faça uso do fork.
  - Após concluir o desafio commite os arquivos do seu código na sua branch para disponibilizá-la para revisão.